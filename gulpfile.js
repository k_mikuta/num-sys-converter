var gulp = require('gulp');
var browserSync = require('browser-sync');
var wiredep = require('gulp-wiredep');
var jshint = require('gulp-jshint');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var Server = require('karma').Server;
var minifycss = require('gulp-minify-css');
var templateCache = require('gulp-angular-templatecache');

gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: './'
    }
  });
});

gulp.task('bs-reload', function () {
  browserSync.reload();
});

gulp.task('styles', function(){
  gulp.src(['src/styles/*.css'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(gulp.dest('dist/styles/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('dist/styles/'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('scripts', function(){
  return gulp.src('src/scripts/*.js')
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('num-sys-converter.js'))
    .pipe(gulp.dest('dist/scripts/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('dist/scripts/'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('template-cache', function () {
  return gulp.src('src/*.html')
    .pipe(templateCache('num-sys-converter.templates.min.js', {
      module: 'numeralSystemConverter',
      standalone: false
    }))
    .pipe(gulp.dest('./dist/scripts'));
});

gulp.task('wiredep', function () {
  gulp.src(['./index.html', 'karma.conf.js'])
    .pipe(wiredep({
      exclude: ['jquery']
    }))
    .pipe(gulp.dest('./'));
});

gulp.task('jshint', function() {
  return gulp.src('./src/scripts/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'));
});

gulp.task('test', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});

gulp.task('tdd', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js'
  }, done).start();
});

gulp.task('install', ['wiredep', 'scripts', 'styles', 'template-cache']);

gulp.task('default', ['browser-sync'], function(){
  gulp.watch('src/styles/**/*.css', ['styles']);
  gulp.watch('src/scripts/**/*.js', ['scripts']);
  gulp.watch('src/*.html', ['template-cache', 'bs-reload']);
  gulp.watch('*.html', ['bs-reload']);
});
