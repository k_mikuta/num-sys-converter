(function() {
'use strict';

  angular
    .module('numeralSystemConverter', [])
    .controller('NumSysConverterController', NumSysConverterController);

  NumSysConverterController.$inject = ['$scope', '$timeout', 'NumSysConverter'];
  function NumSysConverterController($scope, $timeout, NumSysConverter) {
    $scope.formData = {};
    $scope.results = null;
    $scope.numToConvRegex = '';

    $scope.convert = convert;
    $scope.reset = reset;
    $scope.toggleSystems = toggleSystems;
    $scope.validateNumberToConvert = validateNumberToConvert;

    function convert(isValid) {
      if(isValid) {
        var converter = new NumSysConverter($scope.formData.inSystem, $scope.formData.outSystem);
        $scope.results = converter.convert(prepareNumerToConvert());
      }

      function prepareNumerToConvert() {
        if($scope.formData.inSystem === converter.DECIMAL_SYSTEM) {
          return parseInt($scope.formData.numberToConvert);
        }
        return $scope.formData.numberToConvert;
      }
    }

    function reset() {
      $scope.formData = {};
      $scope.results = null;
      $timeout(resetForm);

      function resetForm() {
        $scope.form.$setPristine();
      }
    }

    function toggleSystems() {
      var temp = $scope.formData.inSystem;
      $scope.formData.inSystem = $scope.formData.outSystem;
      $scope.formData.outSystem = temp;
    }

    function validateNumberToConvert() {
      var PATTERN = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var legalChars = PATTERN.substring(0, $scope.formData.inSystem);
      $scope.numToConvRegex = '[' + legalChars + ']*';
    }
  }
})();
