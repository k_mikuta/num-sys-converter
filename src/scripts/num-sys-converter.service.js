(function() {
  'use strict';

  angular
    .module('numeralSystemConverter')
    .factory('NumSysConverter', NumSysConverter);

  function NumSysConverter() {
    var NumSysConverter = function(inSys, outSys) {
      var ILLEGAL_ARGUMENT_ERROR_MESSAGE = 'Illegal argument passed, should be a natural number.';
      var PATTERN = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

      var that = this;
      this.DECIMAL_SYSTEM = 10;
      this.inputSystem = validateNatural(inSys);
      this.outputSystem = validateNatural(outSys);

      this.convert = convert;

      function convert(valueToConvert) {
        var result = null;
        if(that.inputSystem === that.DECIMAL_SYSTEM) {
          var decimalToConvert = validateNatural(valueToConvert);
          result = convertFromDecimal(decimalToConvert);
        } else {
          result = convertToDecimal(valueToConvert);
        }

        return result;
      }

      function convertFromDecimal(decimalToConvert) {
        var result = '';

        while(decimalToConvert > 0) {
          var modResult = decimalToConvert % that.outputSystem;
          result = PATTERN[modResult] + result;
          decimalToConvert = parseInt(decimalToConvert / that.outputSystem);
        }

        return result;
      }

      function convertToDecimal(valueToDecimal) {
        var result = 0;

        var temp = 1;
        for(var i=valueToDecimal.length-1; i>= 0; i--) {
          var intValue = PATTERN.indexOf(valueToDecimal[i]);
          result = result + (intValue * temp);
          temp *= that.inputSystem;
        }

        return result;
      }

      function validateNatural(arg) {
        var errorCondition = !arg || isNaN(arg) || arg < 1 || (parseInt(arg, 10) !== arg);
        if(errorCondition) {
          throw new Error(ILLEGAL_ARGUMENT_ERROR_MESSAGE);
        }
        return arg;
      }
    };

    return NumSysConverter;
  }
})();
