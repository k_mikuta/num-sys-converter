(function () {
  'use strict';

  angular
    .module('numeralSystemConverter')
    .directive('numSysConverter', numSysConverter);

  function numSysConverter() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'num-sys-converter.view.html',
      controller: 'NumSysConverterController'
    };
    return directive;
  }
})();

