(function () {
  'use strict';

  describe('numSysConverver', function () {
    var NumSysConverter;

    beforeEach(module('numeralSystemConverter'));

    beforeEach(inject(function(_NumSysConverter_) {
      NumSysConverter = _NumSysConverter_;
    }));

    it('should be defined', function() {
      expect(NumSysConverter).toBeDefined();
    });

    describe('constructor', function() {
      it('should throw error, when there is no arguments', function() {
        expect(function() {
          new NumSysConverter();
        }).toThrow(new Error('Illegal argument passed, should be a natural number.'));
      });

      it('should throw error, when argument is not integer', function() {
        expect(function() {
          new NumSysConverter(2, 2.5);
        }).toThrow(new Error('Illegal argument passed, should be a natural number.'));
      });

      it('should init values', function() {
        // Given
        var inSys = 10;
        var outSys = 2;

        // When
        var converter = new NumSysConverter(inSys, outSys);

        // Then
        expect(converter.inputSystem).toBe(inSys);
        expect(converter.outputSystem).toBe(outSys);
      });
    });

    describe('convert', function() {
      it('should be defined', function() {
        // When
        var converter = new NumSysConverter(1, 2);

        // Then
        expect(converter.convert).toBeDefined();
      });

      it('should throw error', function() {
        // Given
        var inputSystem = 10;
        var outputSystem = 2;
        var converter = new NumSysConverter(inputSystem, outputSystem);

        // When
        function callConvert() {
          converter.convert(24.5);
        }

        // Then
        expect(callConvert)
          .toThrow(new Error('Illegal argument passed, should be a natural number.'))
      });

      /* DEC -> BIN | BIN -> DEC */
      it('should convert decimal to bin', function() {
        var config = {
          inputSystem: 10,
          outputSystem: 2,
          valueToConvert: 29,
          expectedResult: '11101'
        };

        runTestingConvertMethod(config);
      });

      it('should convert bin to decimal', function() {
        var config = {
          inputSystem: 2,
          outputSystem: 10,
          valueToConvert: '11101',
          expectedResult: 29
        };

        runTestingConvertMethod(config);
      });

      /* DEC -> HEX | HEX -> DEC */
      it('should convert decimal to hex', function() {
        var config = {
          inputSystem: 10,
          outputSystem: 16,
          valueToConvert: 450,
          expectedResult: '1C2'
        };

        runTestingConvertMethod(config);
      });

      it('should convert hex to decimal', function() {
        var config = {
          inputSystem: 16,
          outputSystem: 10,
          valueToConvert: '1C2',
          expectedResult: 450
        };

        runTestingConvertMethod(config);
      });

      /* DEC -> OCT | OCT -> DEC */
      it('should convert decimal to oct', function() {
        var config = {
          inputSystem: 10,
          outputSystem: 8,
          valueToConvert: 230,
          expectedResult: '346'
        };

        runTestingConvertMethod(config);
      });

      it('should convert oct to decimal', function() {
        var config = {
          inputSystem: 8,
          outputSystem: 10,
          valueToConvert: '346',
          expectedResult: 230
        };

        runTestingConvertMethod(config);
      });

      /* DEC -> 20 | 20 -> DEC */
      it('should convert decimal to 20', function() {
        var config = {
          inputSystem: 10,
          outputSystem: 20,
          valueToConvert: 650,
          expectedResult: '1CA'
        };

        runTestingConvertMethod(config);
      });

      it('should convert 20 to decimal', function() {
        var config = {
          inputSystem: 20,
          outputSystem: 10,
          valueToConvert: '1CA',
          expectedResult: 650
        };

        runTestingConvertMethod(config);
      });

      function runTestingConvertMethod(config) {
        // Given
        var converter = new NumSysConverter(config.inputSystem, config.outputSystem);

        // When
        var result = converter.convert(config.valueToConvert);

        // Then
        expect(result).toBe(config.expectedResult);
      }
    });
  });
})();
