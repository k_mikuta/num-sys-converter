(function () {
  'use strict';

   var controller;
   var scope;

  describe('NumSysConverterController', function() {
    beforeEach(module('numeralSystemConverter'));

    beforeEach(inject(function($controller){
      scope = {};
      controller = $controller('NumSysConverterController', { $scope: scope });
    }));

    it('should be defined', function() {
      expect(controller).toBeDefined();
    });

    describe('formData', function() {
      it('should be initialized with empty object', function() {
        expect(scope.formData).toEqual({});
      });
    });

    describe('results', function() {
      it('should be initialized with null', function() {
        expect(scope.results).toBeNull();
      });
    });

    describe('convert', function() {
      it('should be defined', function() {
        expect(scope.convert).toBeDefined();
      });

      it('should set value for results, when form is valid', function () {
        // Given
        scope.formData.inSystem = 2;
        scope.formData.outSystem = 10;
        scope.formData.numberToConvert = '5';

        // When
        scope.convert(true);

        // Then
        expect(scope.results).toEqual(jasmine.any(Number));
      });

      it('should parse numberToConvert to integer', function() {
        // Given
        scope.formData.inSystem = 10;
        scope.formData.outSystem = 2;
        scope.formData.numberToConvert = '5';

        // When
        scope.convert(scope.formData.numberToConvert);
      });
    });

    describe('validateNumberToConvert', function() {
      it('should be defined', function() {
        expect(scope.validateNumberToConvert).toBeDefined();
      });

      it('should generate proper regex', function() {
        // Given
        scope.formData.inSystem = 2;
        scope.formData.numberToConvert = '';
        var expectedResult = '[01]*';
        var secondInMilis = 1000;

        // When
        scope.formData.numberToConvert = '1001';

        // Then
        setTimeout(doAssertion, secondInMilis);

        function doAssertion(){
          expect(scope.numToConvRegex).toEqual(expectedResult)
        }
      });
    });

    describe('reset', function() {
      it('should be defined', function() {
        expect(scope.reset).toBeDefined();
      });

      it('should clear results and formData', function () {
        // Given
        scope.formData = {example: 'value'};
        scope.results = 250;

        // When
        scope.reset();

        // Then
        expect(scope.formData).toEqual({});
        expect(scope.results).toBeNull();
      });
    });

    describe('toggleSystems', function() {
      it('should be defined', function() {
        expect(scope.toggleSystems).toBeDefined();
      });

      it('should toggle values of input system and output system', function() {
        // Given
        scope.formData.inSystem = 10;
        scope.formData.outSystem = 2;

        var expectedInputSystem = 2;
        var expectedOutputSystem = 10;

        // When
        scope.toggleSystems();

        // Then
        expect(scope.formData.inSystem).toBe(expectedInputSystem);
        expect(scope.formData.outSystem).toBe(expectedOutputSystem);
      });
    });
  });
})();
