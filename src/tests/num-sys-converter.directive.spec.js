(function () {
  'use strict';

  describe('num-sys-converter directive', function() {
    var $compile,
        $rootScope;
    var directiveElement;

    beforeEach(module('numeralSystemConverter'));

    beforeEach(inject(function(_$compile_, _$rootScope_) {
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      directiveElement = $compile('<num-sys-converter></num-sys-converter>')($rootScope);
      $rootScope.$digest();
    }));

    it('should contain legend tag with given form title', function() {
      var legend = directiveElement.find('legend');
      expect(directiveElement.html()).toContain('Numeral System Converter');
    });

    it('should contain button of type: submit', function() {
      var form = directiveElement.find('form');
      var buttons = form.find('button');
      var expectedTypes = ['submit', 'reset', 'button'];

      for(var i=0; i<buttons.length; i++) {
        var btnEl = angular.element(buttons[i]);
        expect(btnEl.attr('type')).toEqual(expectedTypes[i]);
      }
    });

    it('should contain reset button executing reset() method', function() {
      var buttons = directiveElement.find('form').find('button');

      for(var i=0; i<buttons.length; i++) {
        var btnEl = angular.element(buttons[i]);
        doTest(btnEl);
      }

      function doTest(btn) {
        if(btn.attr('type') === 'reset') {
          expect(btn.attr('ng-click')).toEqual('reset()');
        }
      }
    });

    it('should contain button executing toggleSystems() method', function() {
      var buttons = directiveElement.find('form').find('button');

      for(var i=0; i<buttons.length; i++) {
        var btnEl = angular.element(buttons[i]);
        doTest(btnEl);
      }

      function doTest(btn) {
        if(btn.attr('type') === 'button') {
          expect(btn.attr('ng-click')).toEqual('toggleSystems()');
        }
      }
    });

    it('should contain 5 form groups', function() {
      var formGroups = directiveElement[0].querySelectorAll('.form-group');
      expect(formGroups.length).toEqual(5);
    });

    it('should contain inputs in first two form groups, which have "min" attribute initialized with "0"', function() {
      var formGroups = directiveElement[0].querySelectorAll('.form-group');
      for(var i=0; i<2; i++) {
        var element = angular.element(formGroups[i]).find('input');
        expect(element.attr('min')).toEqual('1');
      }
    });

    it('should contain inputs with formData model', function() {
      // Given
      var models = ['formData.inSystem', 'formData.outSystem', 'formData.numberToConvert'];

      // When
      var inputs = directiveElement.find('input');

      // Then
      for(var i=0; i<inputs.length; i++) {
        var inputElement = angular.element(inputs[i]);
        expect(inputElement.attr('ng-model')).toBe(models[i])
      }
    });

    it('should contain labels for inputs with proper title', function() {
      var expectedTitles = ['Input system', 'Output system', 'Number to convert', 'Results'];
      var formGroups = directiveElement[0].querySelectorAll('.form-group');

      for(var i=0; i<formGroups.length; i++) {
        var formGroupElement = angular.element(formGroups[i]);
        var label = formGroupElement.find('label');

        expect(label).toBeDefined();
        expect(label.html()).toBe(expectedTitles[i]);
      }
    });

    it('should contain first two inputs with proper types', function() {
      // Given
      var inputTypes = ['number', 'number', 'text'];

      // When
      var inputs = directiveElement.find('input');

      // Then
      for(var i=0; i<inputs.length; i++) {
        var inputElement = angular.element(inputs[i]);
        expect(inputElement.attr('type')).toBe(inputTypes[i]);
      }
    });

    it('should contain hidden <p> tag for results', function() {
      // Given
      var pTagWithResults = directiveElement[0].querySelectorAll('#results');

      // When
      var parentDiv = angular.element(pTagWithResults).parent().parent();

      // Then
      expect(parentDiv.hasClass('ng-hide')).toBeTruthy();
    });

    it('should contain form tag, which call "convert" method on submit', function() {
      var formTag = directiveElement.find('form');
      expect(formTag.attr('ng-submit')).toContain('convert(form.$valid)');
    });
  });

})();
