# README #
This document describes how to launch application and project tools.


### Tools ###

* Install NodeJS with Node Package Manager (NPM) 
* Install Bower
* Install Gulp CLI

### Before run ###

In project's directory open terminal and run commands:

* `npm install` - to download dev tools' dependencies
* `bower install` - to download frontend libraries
* `gulp install` - to generate dist scripts

### Launch and development

In project's directory open terminal and run commands:

* `gulp` - run local http server
* `gulp tdd` - run tests
* `gulp jshint` - run code quality tool
* `gulp wiredep` - inject Bower dependencies into HTML and KarmaJS config file.


To check list of all available commands, see gulpfile.js